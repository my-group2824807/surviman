using UnityEngine;
using UnityEngine.Advertisements;

public class AdsInitializer : MonoBehaviour, IUnityAdsInitializationListener
{
    [SerializeField] string androidGameID = "5316355";
    [SerializeField] string iOSGameID = "5316354";
    [SerializeField] bool testMode = false;
    private string gameID;

    private void Awake()
    {
        gameID = (Application.platform == RuntimePlatform.IPhonePlayer) ? iOSGameID : androidGameID;
        Advertisement.Initialize(gameID, testMode, this);
    }


    public void OnInitializationComplete()
    {
        Debug.Log("������������� ������ �������.");
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"������ �������������: {error.ToString()} - {message}");
    }

}
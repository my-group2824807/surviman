using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] public float _speed;
    private int _numberOfCoinsInLevel;
    private float _oldMousePositionX;
    private float _eulerY;

    //  private float timePressed = 0f; // ������ ������ ������
    [SerializeField] private Animator _animatorTimmy;

    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    public float timeLeft = 1.0f;

    private Vector2 fingerDown; // ������� ������ ��� ������ ������
    private Vector2 fingerUp; // ������� ������ ��� ��������� ������
    public bool detectSwipeOnlyAfterRelease = false; // ����������, ������ �� ������ ������������ ����� ������ ����� ���������� ������
    public float SWIPE_THRESHOLD = 20f; // ��������� �������� ��� ����������� ������
    float touchTime = 0f;
    float SledPos;
    float timer;
   
    private bool isDoubleTap = false; // ����, �����������, ��� ��� ������ ������� ���
    private float tapTimer = 0f; // ������, ������������� �������� ����� ������
    private float tapInterval = 0.3f; // ������������ �������� ����� ������ � �������� ��� ���������� �������� ����



    void Update()
    {       
        if (Input.GetKeyDown(KeyCode.Escape)) ///�����
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }

       /* if (Input.GetMouseButtonDown(0))
        {
            _oldMousePositionX = Input.mousePosition.x;
            _animatorTimmy.SetBool("Run", true);
        }*/

        _animatorTimmy.SetBool("Run", true);
        Vector3 Position = transform.position;
        
        SledPos = Position.x;
        Position.x = Mathf.Clamp(SledPos, -2.6f, 2.6f);
        transform.position = Position;
        transform.position += new Vector3(0f, 0f, _speed * Time.deltaTime);


        /*  if (Input.GetMouseButtonUp(0))
          {
              _animatorTimmy.SetBool("Run", false);
          }*/

        //������� ������ ������ ������     ��� (�����)

        if (Input.GetKey(KeyCode.Space))
        {
            _numberOfCoinsInLevel = FindObjectOfType<CoinManager>()._numberOfCoinsInLevel;

            if (_numberOfCoinsInLevel > 0)
            {
                _speed = 20;
                timer += Time.deltaTime;
                if (timer >= 0.2f)
                {
                    timer = 0f;
                    _numberOfCoinsInLevel -= 1;

                    FindObjectOfType<CoinManager>()._numberOfCoinsInLevel = _numberOfCoinsInLevel;
                    FindObjectOfType<CoinManager>()._text.text = _numberOfCoinsInLevel.ToString();
                }
            }
            else _speed = 10;
        }

        if (Input.GetKeyUp(KeyCode.Space)) //������ ���������
        {
            _speed = 10;
        }


        //// ���������� ������      
        foreach (Touch touch in Input.touches)
        { // ������������ ��� ������� ������
            
            if (touch.phase == TouchPhase.Began)                                    // ���� ����� ������ ��� �������� ������
            { 
                fingerUp = touch.position;                                          // ���������� ������� ������ ��� ������ ������
                fingerDown = touch.position;                                        // ���������� ������� ������ ��� ������ ������
                touchTime = Time.time;                                                // ���������� ����� ������ �������
            }

           
                       
           
            if (touch.phase == TouchPhase.Ended)                         // ���� ����� ��� ����� �� ������
            {
                _speed = 10;             
                fingerDown = touch.position;                             // ���������� ������� ������ ��� ��������� ������
                var direction = fingerUp - fingerDown;                   // ���������� ����������� ������
                if (direction.magnitude > SWIPE_THRESHOLD)
                {                                                        // ���� ����� ������ ������ ���������� ��������
                    var swipeDirection = Vector2.Dot(Vector2.right, direction.normalized); // ���������� ����������� ������ (����� ��� ������)
                    if (swipeDirection > 0)                               // ���� ����������� ������ �����
                    {                       
                        Vector3 position = transform.position;
                        if (position.x > 2.6f)
                        {
                            SledPos = 2.6f;
                        }
                        if ((position.x <= 2.6f) && (position.x > 1f))
                        {
                            SledPos = 0f;
                        }
                        if ((position.x <= 1f) && (position.x >= -1f))
                        {
                            SledPos = -2.5f;
                        }
                        position.x = SledPos;
                        transform.position = position;
                        transform.position += new Vector3(0f, 0f, _speed * Time.deltaTime);

                    }
                    if (swipeDirection < 0)
                    { // ���� ����������� ������ ������             
                        Vector3 position = transform.position;
                        if (position.x < -2.6f)
                        {
                            SledPos = -2.6f;
                        }
                        if ((position.x >= -2.6f) && (position.x < -1f))
                        {
                            SledPos = 0f;
                        }
                        if ((position.x >= -1f) && (position.x <= 1f))
                        {
                            SledPos = 2.5f;
                        }

                        position.x = SledPos;
                        transform.position = position;
                        transform.position += new Vector3(0f, 0f, _speed * Time.deltaTime);
                    }
                }
            }

            if (touch.phase == TouchPhase.Stationary)
            {
               
                _numberOfCoinsInLevel = FindObjectOfType<CoinManager>()._numberOfCoinsInLevel;

                if (_numberOfCoinsInLevel > 0)
                {
                    _speed = 20;
                    timer += Time.deltaTime;
                    if (timer >= 0.5f)
                    {
                        timer = 0f;
                        _numberOfCoinsInLevel -= 1;

                        FindObjectOfType<CoinManager>()._numberOfCoinsInLevel = _numberOfCoinsInLevel;
                        FindObjectOfType<CoinManager>()._text.text = _numberOfCoinsInLevel.ToString();
                    }
                }
                else _speed = 10;
            }

            
        }
       
        
        if (Input.GetKeyDown(KeyCode.LeftArrow))           // ���� ����������� ������ �����
        {
                     
            Vector3 position = transform.position;
            if (position.x > 2.6f) 
            {
                SledPos = 2.6f;                
            }
            if ((position.x <= 2.6f) && (position.x > 1f))
            {
                SledPos = 0f;
            }
            if ((position.x <= 1f) && (position.x >= -1f))
            {
                SledPos = -2.5f;
            }
            position.x = SledPos;
            transform.position = position;
            transform.position += new Vector3(0f, 0f, _speed * Time.deltaTime);

        }
       
        if (Input.GetKeyDown(KeyCode.RightArrow))
        { // ���� ����������� ������ ������                     
            Vector3 position = transform.position;
            if (position.x < -2.6f)
            {
                SledPos = -2.6f;
            }
            if ((position.x >= -2.6f) && (position.x < -1f))
            {
                SledPos = 0f;               
            }
            if ((position.x >= -1f) && (position.x <= 1f))
            {
                SledPos = 2.5f;
            }

            position.x = SledPos;
            transform.position = position;
            transform.position += new Vector3(0f, 0f, _speed * Time.deltaTime);
        }

                                                                                       ////////////////// ������� ��� //////////////
   
        if (Input.touchCount > 0)
        {
            // �������� ������ ������� ������
            Touch touch = Input.GetTouch(0);

            // ���� ������� ��������
            if (touch.phase == TouchPhase.Began)
            {
                // ���� ������ ���� �� �����
                if (tapTimer > 0)
                {
                    // ������������� ���� �������� ���� � true
                    isDoubleTap = true;
                }
                else
                {
                    // ���������� ���� �������� ���� � false
                    isDoubleTap = false;
                }

                // ���������� ������ ����
                tapTimer = tapInterval;
            }
        }

        // ���� ������ ���� ������ ����
        if (tapTimer > 0)
        {
            // ��������� ��� �� ��������� ����� � ����������� �����
            tapTimer -= Time.deltaTime;
        }

        // ���� ��� ������ ������� ���
        if (isDoubleTap || Input.GetKeyDown(KeyCode.DownArrow))
        {
            _numberOfCoinsInLevel = FindObjectOfType<CoinManager>()._numberOfCoinsInLevel;

            if (_numberOfCoinsInLevel > 3)
            {
                Time.timeScale = 0.4f;
                timer += Time.deltaTime;
                if (timer >= 0.001f)
                {
                    timer = 0f;
                    _numberOfCoinsInLevel -= 1;

                    FindObjectOfType<CoinManager>()._numberOfCoinsInLevel = _numberOfCoinsInLevel;
                    FindObjectOfType<CoinManager>()._text.text = _numberOfCoinsInLevel.ToString();
                }
            }
            else Time.timeScale = 1f;
        }

        if (Input.GetKeyUp(KeyCode.DownArrow)) //"������� ����" ���������
        {
            Time.timeScale = 1f;
        }





       /* if (isDoubleTap || Input.GetKey(KeyCode.DownArrow))
            {
                // Time.timeScale = 1f;
                _numberOfCoinsInLevel = FindObjectOfType<CoinManager>()._numberOfCoinsInLevel;

                if (_numberOfCoinsInLevel > 3)
                {
                    Time.timeScale = 0.4f;
                    timer += Time.deltaTime;
                Debug.Log(timer);
                    if (timer <= 0.5f)
                    {
                        timer = 0f;
                        _numberOfCoinsInLevel -= 1;

                        FindObjectOfType<CoinManager>()._numberOfCoinsInLevel = _numberOfCoinsInLevel;
                        FindObjectOfType<CoinManager>()._text.text = _numberOfCoinsInLevel.ToString();
                    }
                }
                else
                {
                    Time.timeScale = 1f;
                    isDoubleTap = false;
                }

            } */
        
       
    }



    public void Down()
    {
        transform.Rotate(0f, 90f, 0f);
        _animatorTimmy.SetTrigger("Down");

    }

   
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinManager : MonoBehaviour
{

    [SerializeField] public int _numberOfCoinsInLevel;
    [SerializeField] public TextMeshProUGUI _text;
 
    public void AddOne()
    {
        _numberOfCoinsInLevel += 1;
        _text.text = _numberOfCoinsInLevel.ToString();
       

    }

}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    [SerializeField] GameObject _startMenu;
    [SerializeField] TextMeshProUGUI _levelText;
    [SerializeField] TextMeshProUGUI _SpeedText;
    [SerializeField] TextMeshProUGUI _FPSText;
    [SerializeField] GameObject _finishWindow;
    [SerializeField] GameObject _LoseWindow;
      [SerializeField] GameObject _InterstitialAds;
    private bool isPaused = false;
    


    IEnumerator FPS()
    {
        while (true)
        {
            float fps = 1f / Time.deltaTime;
            _FPSText.text = "FPS: " + Mathf.RoundToInt(fps).ToString();
            yield return new WaitForSeconds(1f);
        }
    }


    private void Start()
    {
        StartCoroutine(FPS());
        _levelText.text = SceneManager.GetActiveScene().name;
    }

    void Update()
    {
        GameObject obj = GameObject.Find("Player");
        _SpeedText.text = "Speed: " + obj.GetComponent<PlayerMove>()._speed.ToString();


       /* fpsSum += 1f / Time.deltaTime;                 ///����������� fps ��� � 2 ������
        fpsCount++;

        if (Time.time >= 2f)
        {
            fpsAverage = fpsSum / fpsCount;
            _FPSText.text = "FPS: " + Mathf.RoundToInt(fpsAverage).ToString();
            fpsSum = 0f;
            fpsCount = 0;
        }*/



    }

            public void Play()
    {
        _startMenu.SetActive(false);
        FindObjectOfType<PlayerBehaviour>().Play();
        FindObjectOfType<FatManMagn>().Run();
        Time.timeScale = 1;
    }
    public void ShowFinishWindow()
    {
        _finishWindow.SetActive(true);
        Time.timeScale = 0;
    }
    public void ReLoad()
    {

         FindObjectOfType<InterstitialAds>().ShowAd();                      //���������� ������� Interstitial
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void ShowLoseWindow()
    {
      /*  GameObject _camera = GameObject.Find("Main Camera");
        _camera.GetComponent<CameraMove>().Povorot(); */
        _LoseWindow.SetActive(true);      
        Time.timeScale = 0;
    }

    public void NextLevel()
    {
        FindObjectOfType<RewardedAds>().ShowAd();                          //���������� ������� Rewarded
        int next = SceneManager.GetActiveScene().buildIndex + 1;
        if (next < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(next);
        }


    }

    public void Pause()
    {
        if (isPaused)
        {
            Time.timeScale = 1;
            isPaused = false;
        }
        else
        {
            Time.timeScale = 0;
            isPaused = true;
        }


    }
}
